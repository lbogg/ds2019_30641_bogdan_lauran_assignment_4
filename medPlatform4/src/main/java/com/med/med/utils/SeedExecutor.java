package com.med.med.utils;

import com.med.med.models.Role;
import com.med.med.models.User;
import com.med.med.models.UserActivity;
import com.med.med.models.data.UserActivityDao;
import com.med.med.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Component
public class SeedExecutor {
    private final UserDao userDao;
    private final UserActivityDao userActivityDao;
    private final ActivityReader activityReader;

    @Autowired
    public SeedExecutor(UserDao userDao, UserActivityDao userActivityDao, ActivityReader activityReader) {
        this.userDao = userDao;
        this.userActivityDao = userActivityDao;
        this.activityReader = activityReader;
    }

    @PostConstruct
    public void doSeed() {
        User user = new User();
        user.setName("John Doe");
        user.setPassword("parola");
        user.setRole(Role.PATIENT);
        user.setUsername("johndoe");

        User user2 = new User();
        user2.setName("Jane Doe");
        user2.setPassword("parola");
        user2.setRole(Role.PATIENT);
        user2.setUsername("janedoe");

        User doctor = new User();
        doctor.setName("Doctor Doctorescu");
        doctor.setPassword("parola");
        doctor.setRole(Role.DOCTOR);
        doctor.setUsername("doctor");

        userDao.saveAll(Arrays.asList(user, user2, doctor));

        List<UserActivity> userActivities = activityReader.doRead();
        for (UserActivity userActivity : userActivities) {
            long chance = Math.round(Math.random() * 2);

            if (chance == 0) {
                userActivity.setUser(user);
            } else {
                userActivity.setUser(user2);
            }
        }

        userActivityDao.saveAll(userActivities);
    }
}
